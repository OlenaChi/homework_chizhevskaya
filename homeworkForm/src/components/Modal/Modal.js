import styles from './Modal.module.scss';
import PropTypes from 'prop-types'
import Button from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {setIsOpenModal} from "../../store/slices/modalSlices";
import {incrementCounterAdd} from "../../store/slices/counterAddSlices";
import {addListBasket, delListBasketItem} from "../../store/slices/listBasketSlices";
import {decrementCounterAdd} from "../../store/slices/counterAddSlices";

const Modal =(

) => {
    const dispatch = useDispatch()
    const isOpenModal = useSelector(store => store.modal.isOpenModal)
    const typeOfModal = useSelector(store => store.modal.typeOfModal)
    const infoModal = useSelector(store => store.modal.infoModal)
    console.log(infoModal)
    if(!isOpenModal){
    return null
}
        return (
            <>
                <div className={styles.modal}>
                    <div  className={styles.modalBackground}
                          onClick={()=>dispatch(setIsOpenModal(false))}
                    />

                    <div className={styles.modalMainContainer} >
                        <button className = {styles.btnX}
                                onClick={()=>dispatch(setIsOpenModal(false))}
                        >X</button>
                        <div className={styles.modalContentWrapper}>
                            {typeOfModal && <><header className={styles.modalHeader}>
                                Подобається
                                {infoModal.name}
                                ?
                            </header>
                                <div className={styles.modalBody}>Треба брати</div></>
                            }
                            {!typeOfModal && <><header className={styles.modalHeader}>
                             Вже не подобається
                                {infoModal.name}
                                ?
                            </header>
                                <div className={styles.modalBody}>Видаляємо з корзини?</div></>
                            }

                        </div>
                        <div className={styles.modalButtonWrapper}>
                            <Button style={styles.btnModal} onClick={()=>dispatch(setIsOpenModal(false))} > Ще подумаю</Button>
                            {typeOfModal && <Button style={styles.btnModal} onClick={() => {
                                dispatch(addListBasket(infoModal))
                                dispatch(incrementCounterAdd());
                                dispatch(setIsOpenModal(false))
                            }}> Авжеж беру</Button>}

                            {!typeOfModal && <Button style={styles.btnModal} onClick={() => {
                                dispatch(decrementCounterAdd(infoModal.countItem))
                                dispatch(delListBasketItem(infoModal))
                                dispatch(setIsOpenModal(false))
                            }}> Видаляємо</Button>}

,
                        </div>
                    </div>
                </div>


            </>
        )

}

// Modal.propTypes = {
//     isOpenModal: PropTypes.bool.isRequired,
//     toggleModal: PropTypes.func.isRequired,
//     modalType: PropTypes.bool.isRequired,
//     incrementAdd: PropTypes.func.isRequired,
//     infoModal:PropTypes.object.isRequired,
//     decrementAdd: PropTypes.func.isRequired,
// }

export default Modal;