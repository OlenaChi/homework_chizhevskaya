import {NavLink} from 'react-router-dom'
const Navigation =({favourite, basket, cottonFlower, style})=>{

    return (
<nav>
    <ul>
        <li className={`${style}`}>
            <NavLink to = "/">{cottonFlower}</NavLink>
        </li>
        <li className={`${style}`}>
            <NavLink to = "/favourite">{favourite}</NavLink>
        </li>
        <li className={`${style}`}>
            <NavLink to = "/basket">{basket}</NavLink>
        </li>
    </ul>

</nav>
    )
}
export default Navigation
