import {BrowserRouter} from "react-router-dom";
import Header from "./components/Header/Header";
import AppRoutes from "./AppRoutes";
import Modal from "./components/Modal/Modal";
import './App.css';
import {Provider, useDispatch} from "react-redux";
import {store} from "./store";
import {useEffect} from "react";
import {createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import getData from "./api/getData";

import {fetchData} from "./store/slices/dataSlices";
import {fetchCounterAdd} from "./store/slices/counterAddSlices";
import {fetchCounterFavourite} from "./store/slices/counterFavouriteSlices";
import {fetchListBasket} from "./store/slices/listBasketSlices";
import {fetchListFavourite} from "./store/slices/listFavouriteSlices";
// import {fetchCounterAdd} from "./store/slices/counterAddSlice";



function App() {
const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(fetchData());
        dispatch(fetchCounterAdd());
        dispatch(fetchCounterFavourite());
        dispatch(fetchListBasket());
        dispatch(fetchListFavourite())

    }, [])

  return (


        <div className="App">
          <Header />
          <AppRoutes />
          <Modal />
        </div>



  );
}

export default App;
