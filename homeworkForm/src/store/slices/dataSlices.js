import {createSlice} from "@reduxjs/toolkit";
import {getData} from "../../api/getData";

const dataSlices = createSlice({
    name:"data",
    initialState:{
        data:[]
    },
    reducers:{
        addData:(state, action)=>{
            state.data = action.payload;


        },
        setFavourite:(state,action)=>{
          const index = state.data.findIndex(el => el.id === action.payload.id)
          state.data[index].isFavourite = true
          localStorage.setItem("data", JSON.stringify(state.data))
        },
        setNotFavourite:(state, action)=>{
            const index = state.data.findIndex(el => el.id === action.payload.id)
            state.data[index].isFavourite = false
            localStorage.setItem("data", JSON.stringify(state.data))
        }
    }
});

export const fetchData = () => async (dispatch) => {
    const data = await getData();

    dispatch(addData(data));

}

export default dataSlices.reducer;
export const {addData, setFavourite, setNotFavourite} = dataSlices.actions;