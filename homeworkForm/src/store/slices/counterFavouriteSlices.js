import {createSlice} from "@reduxjs/toolkit";
import {getCounterFavourite} from "../../api/getData";

const counterFavouriteSlices = createSlice({
    name:"counterFavourite",
    initialState:{
        counterFavourite:0
    },
    reducers:{
        setCounterFavourite:(state, action)=>{
            state.counterFavourite = action.payload;

        },
        incrementCounterFavourite:(state )=>{
                state.counterFavourite +=1;
                localStorage.setItem('counterFavourite', JSON.stringify(state.counterFavourite))
            },

        decrementCounterFavourite:(state)=>{
                state.counterFavourite -=1;
                localStorage.setItem('counterFavourite', JSON.stringify(state.counterFavourite))
            }
        }
});

export const fetchCounterFavourite = () => async (dispatch) => {
    const counterAdd = await getCounterFavourite();
    counterAdd? dispatch(setCounterFavourite(counterAdd)) : dispatch(setCounterFavourite(0))


}
export default counterFavouriteSlices.reducer
export const{setCounterFavourite, incrementCounterFavourite, decrementCounterFavourite} = counterFavouriteSlices.actions