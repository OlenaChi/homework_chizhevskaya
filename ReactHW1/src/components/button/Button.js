import React from "react";
import styles from "./Button.module.scss"
console.log(styles)

class Button extends React.PureComponent {

    render() {
        const { children, type, handleClick, backgroundColor, border} = this.props;

        return (
            <>

                <button style ={{backgroundColor, border}} onClick={handleClick} className={styles.button} type = {type ? type : 'button'}> {children}</button>
            </>
        )
    }
}
export default Button;