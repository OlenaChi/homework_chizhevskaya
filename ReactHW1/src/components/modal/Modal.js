import React from "react";
import styles from "./Modal.module.scss"



class Modal extends React.PureComponent {

    render() {
        const {action, title, text, handleClick, style, closeBtn} = this.props;

        return (
            <>
<div className={styles.modal}>
    <div onClick={handleClick} className={styles.modalBackground} />

      <div className={`${styles.modalMainContainer} ${style}`} >
        <div className={styles.modalContentWrapper}>
            <header className={styles.modalHeader}>
                {title}</header>
            <div className={styles.modalBody}>{text}</div>
        </div>
        <div className={styles.modalButtonWrapper}> {action}</div>
          {closeBtn}
        {/*<div style={closeBtn}  className={styles.modalClose}>X</div>*/}
    </div>
</div>


            </>
        )
    }
}
export default Modal;