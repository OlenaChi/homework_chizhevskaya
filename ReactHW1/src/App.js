import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";
import './App.scss';
import {Component, Fragment} from "react";
const modalInfo = {
    titleFirst: 'Do you want to delete this file?',
    textFirst:'Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it? ',
    titleSecond: 'You enjoy checking my homework, don`t you?',
    textSecond:'I hope:) ',

}



class App extends Component {
    state={
        isOpenModalFirst:false,
        isOpenModalSecond:false,

        closeButton:true,

    }

    openModalFirst=()=>{this.setState({isOpenModalFirst: true})}
    openModalSecond=()=>{this.setState({isOpenModalSecond:true})}
    closeModalFirst=()=>{this.setState({isOpenModalFirst:false})}
    closeModalSecond=()=>{this.setState({isOpenModalSecond:false})}

    render() {


const {isOpenModalFirst, isOpenModalSecond, closeButton} = this.state;
    return (
        <>
        <div className="App">
           <Button backgroundColor = 'Tomato' border = 'Tomato' handleClick = {this.openModalFirst}>Open first modal</Button>
            {isOpenModalFirst && <Modal style={'first_modal'} handleClick={this.closeModalFirst}  title = {modalInfo.titleFirst}  text = {modalInfo.textFirst} action={
                <>
                    <button className = 'first_modal_btn' onClick={this.closeModalFirst}>Ok</button>
                    <button className = 'first_modal_btn' onClick={this.closeModalFirst}>Cancel</button>
                </>
            }
closeBtn={closeButton && <div className='modal-close'>X</div>}

            />}
            <Button backgroundColor = 'Deeppink' border = "Deeppink" handleClick = {this.openModalSecond}>Open second modal</Button>
            {isOpenModalSecond && <Modal style={'second_modal'} backgroundColor = "Deeppink" handleClick={this.closeModalSecond}  title = {modalInfo.titleSecond}  text = {modalInfo.textSecond} action = {
                <div>
                    <button className = 'second_modal_btn second_modal_btn_ugly' onClick={this.closeModalSecond}>Ugly job</button>
                    <button className = 'second_modal_btn second_modal_btn_good' onClick={this.closeModalSecond}>Not bad</button>
                </div>
            } />}
        </div>
        </>)

    }

}

export default App;
