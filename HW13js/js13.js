const buttonBlue = document.createElement('button')
buttonBlue.style.cssText = 'position: absolute; top: 70px; right: 70px; width: 120px; border: 1px solid blue; border-radius: 15%; color: blue; text-align: center;'
buttonBlue.textContent = 'theme blue'
document.body.prepend(buttonBlue)
const buttonGrey = document.createElement('button')
buttonGrey.style.cssText = 'position: absolute; top: 70px; right: 70px; width: 120px; border: 1px solid grey; border-radius: 15%; color: grey; text-align: center;'
buttonGrey.textContent = 'theme grey'

const changeCss = document.createElement('link')
changeCss.innerHTML = `<link rel="stylesheet" href="./style2.css">`
buttonBlue.addEventListener('click',function (){
    document.head.append(changeCss)
    buttonBlue.remove()
    document.body.prepend(buttonGrey)
    localStorage.setItem('theme', 'blue')
    buttonGrey.addEventListener('click', function(){
        localStorage.removeItem('theme')
    })
})
buttonGrey.addEventListener('click', function(){
    localStorage.removeItem('theme')
    changeCss.remove()
})
window.addEventListener('load', function (){
    if(localStorage.getItem('theme')!== null){
        document.head.append(changeCss)
        buttonBlue.remove()
        document.body.prepend(buttonGrey)
    }
})