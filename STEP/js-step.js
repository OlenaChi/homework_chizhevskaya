const menuServices = document.querySelector('.our-services-menu')
const arrayServices = document.querySelectorAll(`.our-services-item`)
const arrayContentServices = document.querySelectorAll('.content-item')

menuServices.addEventListener('click',function (event) {
    arrayServices.forEach(el => {
        console.log(el)
        el.classList.remove(`active`)
        })
   event.target.classList.add('active')
    let dataAtr = event.target.getAttribute(`data-button`)
    arrayContentServices.forEach((elem) =>{
       if(elem.getAttribute(`data-content`) === dataAtr){
           elem.style.display = `flex`
       }
       else {elem.style.display = `none`
       }
    })
})

const hover = {
    graphic:`<div class="item-hover">
                    <div class="hover-circles-blok">
                        <div class="hover-circle">
                          <svg class="hover-circle-img" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                          </svg>
                        </div>
                        <div  class="hover-circle full">
                           <div class="hover-circle-img white-square"></div>
                        </div>
                    </div>
                    <h3 class="hover-title">CREATIVE DESIGN</h3>
                    <h4 class="hover-subtitle">Graphic Design</h4>
                </div>` ,
    web:`<div class="item-hover">
                    <div class="hover-circles-blok">
                        <div class="hover-circle">
                          <svg class="hover-circle-img" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                          </svg>
                        </div>
                        <div  class="hover-circle full">
                           <div class="hover-circle-img white-square"></div>
                        </div>
                    </div>
                    <h3 class="hover-title">AWESOME DESIGN</h3>
                    <h4 class="hover-subtitle">Web Design</h4>
                </div>` ,
    landing: `<div class="item-hover">
                    <div class="hover-circles-blok">
                        <div class="hover-circle">
                          <svg class="hover-circle-img" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                          </svg>
                        </div>
                        <div  class="hover-circle full">
                           <div class="hover-circle-img white-square"></div>
                        </div>
                    </div>
                    <h3 class="hover-title">VARIETY OF PATTERNS</h3>
                    <h4 class="hover-subtitle">Landing Page</h4>
                </div>`,
    wordpress: `<div class="item-hover">
                    <div class="hover-circles-blok">
                        <div class="hover-circle">
                          <svg class="hover-circle-img" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                          </svg>
                        </div>
                        <div  class="hover-circle full">
                           <div class="hover-circle-img white-square"></div>
                        </div>
                        
                    </div>
                    <h3 class="hover-title">FUNCTIONAL</h3>
                    <h4 class="hover-subtitle">Wordpress</h4>
                </div>`,
}

const blockImg = document.querySelector(`.amazing-work-wrapper`)
let a
function showImg(a){
for (let i = a; i < a+3; i++ ){
    blockImg.insertAdjacentHTML(`beforeend`,
        `<div data-img="graphic" class="amazing-work-img">
<img class="img" src="./img/graphic%20design/${i}.jpg" alt="">
${hover.graphic}</div>`)

    blockImg.insertAdjacentHTML(`beforeend`,
        `<div data-img="web" class="amazing-work-img">
<img class="img" src="./img/web%20design/${i}.jpg" alt="">
${hover.web}</div>`)

    blockImg.insertAdjacentHTML(`beforeend`,
        `<div data-img="landing" class="amazing-work-img">
<img class="img" src="./img/landing%20page/${i}.jpg" alt="">
${hover.landing}</div>`)

    blockImg.insertAdjacentHTML(`beforeend`,
        `<div data-img="wordpress" class="amazing-work-img">
<img class="img" src="./img/wordpress/${i}.jpg" alt="">
${hover.wordpress}</div>`)
}}
showImg(1)

const button = document.getElementById(`1`)

console.log(button)
const sectionAmazingWork = document.querySelector(`.our-amazing-work`)
button.addEventListener('click', () =>{
    showImg(4)
    button.remove()
    sectionAmazingWork.insertAdjacentHTML(`beforeend`, `<button class="amazing-work-button" id="2"><img class="img-button" src="./amazing%20work/Forma%201.png" alt="" ><p class="text-button">LOAD MORE</p></button>`)
    const secondButton = document.getElementById(`2`)

    secondButton.addEventListener('click', () =>{

        showImg(7)
        secondButton.remove()
        sectionAmazingWork.style.paddingBottom = '100px'
    })
})

let arrayAmazingWorkMenu = document.querySelector(`.amazing-work-menu`)
let arrayAmazingWorkMenuItem = document.querySelectorAll(`.amazing-work-item`)


arrayAmazingWorkMenu.addEventListener(`click`,function (event) {
    arrayAmazingWorkMenuItem.forEach(el => {
        console.log(el)
        el.classList.remove(`active-item`)
    })
    event.target.classList.add(`active-item`)

    let arrayAmazingWorkItem = document.querySelectorAll(`.amazing-work-img`)


    let dataItem = event.target.getAttribute('data-item')
    if(dataItem === 'all' ){
        arrayAmazingWorkItem.forEach(el => {
            el.style.display = 'block'
        })
    }
    else {arrayAmazingWorkItem.forEach(el =>{
        let dataImg = el.getAttribute(`data-img`)
        if(dataImg === dataItem) {
            console.log (el)
            el.style.display = `block`
        }
        if(dataImg !== dataItem) {
            console.log (el)
        el.style.display = `none`}
    })
    }

})


const swiper = new Swiper('.swiper', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar

});

function customPagination(){
    const paginationItems = document.querySelector(`.person-pagination`).childNodes
    paginationItems.forEach((item,index) =>{
      item.classList.add(`person-pagination-item`)

      item.insertAdjacentHTML (`beforeend`, `<div class="person-pagination-item-img"><img class="person-img"src="./img/people/${++index}.png" alt=""></div>`)

    })

}
customPagination()