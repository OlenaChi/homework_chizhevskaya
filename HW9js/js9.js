const parent = document.querySelector('.tabs')
const array = document.querySelector('.tabs-content')
const  allText = array.children
parent.addEventListener('click',function (event){

    const data = event.target.getAttribute('data-button')

    for (let i = 0; i < allText.length; i++) {
       if(allText[i].getAttribute('data-text') === data){
           allText[i].style.display = 'block'
       }
       if(allText[i].getAttribute('data-text') !== data){
           allText[i].style.display = 'none'
       }

    }
})


