const buttonStop = document.createElement('button')
buttonStop.style.cssText = 'position: absolute; right: 50px; top: 50px; width: 200 px; background: red; color: black; border: 2px solid red'
buttonStop.textContent= 'STOP'
const buttonShow = document.createElement('button')
buttonShow.style.cssText = 'position: absolute; right: 50px; bottom: 50px; width: 200 px; background: green; color: black; border: 2px solid green'
buttonShow.textContent = `SHOW`
buttonShow.disabled = true
document.body.prepend(buttonStop, buttonShow)

const arrayImg = document.querySelectorAll('.image-to-show')
let count=0
let slideImg = setInterval(()=>{
 arrayImg[count].classList.remove('active')
    count++
    if(count>=arrayImg.length){
        count=0
    }
 arrayImg[count].classList.add('active')
},1500)

buttonStop.addEventListener('click',() =>{
       clearInterval(slideImg)
    buttonShow.disabled = false
    //buttonStop.disabled = !buttonStop.disabled
    buttonStop.disabled = true
})
buttonShow.addEventListener('click',() =>{
    buttonStop.disabled = false
    slideImg = setInterval(()=>{
        arrayImg[count].classList.remove('active')
        count++
        if(count>=arrayImg.length){
            count=0
        }
        arrayImg[count].classList.add('active')
    },1500)
})