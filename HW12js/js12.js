document.addEventListener('DOMContentLoaded',function(){
    const img = document.querySelectorAll('.image-to-show')
    console.log(img)
    const buttonStop = document.createElement('button')
    buttonStop.style.cssText = 'position: absolute; right: 50px; top: 50px; width: 200 px; background: red; color: black; border: 2px solid red'
    buttonStop.textContent= 'STOP'
    const buttonShow = document.createElement('button')
    buttonShow.style.cssText = 'position: absolute; right: 50px; bottom: 50px; width: 200 px; background: green; color: black; border: 2px solid green'
    buttonShow.textContent = `SHOW`
    document.body.prepend(buttonStop, buttonShow)
    let i = 0
    let timer = window.setInterval(function(){
       img[i].style.display = 'none'
        i++
        if(i===img.length){
            i=0
        }
        img[i].style.display = 'block'
    },1000)
    buttonStop.addEventListener('click', function() {
        clearInterval(timer)
    })
    buttonShow.addEventListener('click', function (){
        let timer = window.setInterval(function(){
            img[i].style.display = 'none'
            i++
            if(i===img.length){
                i=0
            }
            img[i].style.display = 'block'
        },1000)
    })
})