`use strict`

fetch(`https://ajax.test-danit.com/api/swapi/films`)
    .then(response =>response.json())
    .then  (   data => {
        console.log(data)
            data.forEach(( {id, episodeId, characters:[ ...url], name, openingCrawl})=> {

                new MoviesCard(id,episodeId, name, openingCrawl).render()

                    const promiseArr = [...url].map(url=>fetch(url).then(response=>response.json())

                       .catch(err => console.warn(err)))
                console.log(promiseArr)
                Promise.allSettled([...promiseArr])
                      .then(arrCharacters => {
                          const arrName = arrCharacters.map((el) => el.value.name)
                               console.log(arrName)
                           arrName.forEach(el =>{
                               new ListCharacters(el,id).building()

                          })

                      } )


            } ) })

class MoviesCard {
        constructor(id,episodeId, name, openingCrawl) {
                this.episodeId = episodeId;
                this.name = name;
                this.openingCrawl = openingCrawl;
                this.id = id;

        }
        render(){
            const divCard = document.createElement('div')
            divCard.classList.add(`class${this.id}`)
            divCard.style.cssText = `border: 2px solid #18CFAB;
            border-radius: 20px;
            padding: 5px;
            margin: 5px;
            width: 900px;
            `

            const ulCharacters = document.createElement('ul')
            document.body.append(divCard)

                divCard.insertAdjacentHTML(`beforeend`,
                    `
                <h2>Зоряні війни</h2>
                <h3>Епізод ${this.episodeId}</h3>
                <h4>${this.name} </h4>
                <p> ${this.openingCrawl}</p>
                                               
            `
                )
            divCard.append(ulCharacters)
            ulCharacters.setAttribute('id',`${this.id}`)
        }
}

class ListCharacters {
    constructor(el, id) {
        this.el = el;
        this.id = id;
    }
    building(){
        const ul = document.getElementById(`${this.id}`)
        ul.insertAdjacentHTML(`beforeend`, `
             <li>${this.el}</li>             
                                     `
        )
    }
}



