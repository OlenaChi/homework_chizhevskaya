Promise.all([fetch('https://ajax.test-danit.com/api/json/users').then(response => response.json()),fetch('https://ajax.test-danit.com/api/json/posts').then(response => response.json())] )
    .then(data => {
        console.log(data)
        const arrUser = data[0];
        console.log(arrUser)
        arrUser.forEach(({name, username}) => {
            console.log(name)
            console.log(username)

            new User( name, username).render()
        })
        const arrPosts = data[1];
      arrPosts.forEach(post => {
            arrUser.filter(user=>{
                if(user.id === post.userId){
                    const obj ={
                        ...user,
                        ...post,
                    }
                    const {id,name, username, email, title, body} = obj
                    new Card (id,name, username, email, title, body).createPost()
                }
            })
        })
    })

class User{
    constructor(name, username) {
        this.name = name;
        this.username = username;
    }
    render(){
        const divUserList = document.querySelector('.container-user')
        divUserList.insertAdjacentHTML('beforeend', `
        <div class = 'person'>
        <div class = "avatar"></div>
        <p>${this.name}</p>
        <p>${this.username}</p>
         </div>`)
    }
}

class Card{
    constructor(id,name, username, email, title, body){
       this.id = id;
       this.title = title;
       this.body = body;
       this.name = name;
       this.username = username;
       this.email = email;
       this.card = document.createElement('div')
       this.deleteButton = document.createElement('button')
    }
    createPost(){
       const divCard = document.querySelector('.container-posts')
        divCard.append(this.card)
        this.card.setAttribute('id', `${this.id}`)
        this.card.setAttribute('class', 'div-card')
        this.card.insertAdjacentHTML(`beforeend`, `
        <h3>${this.name}</h3>
        <h2>${this.username}</h2>
        <h4>${this.email}</h4>
        <h1>${this.title}</h1>
        <p>${this.body}</p>`
)
        this.card.append(this.deleteButton)
        this.deleteButton.insertAdjacentHTML('beforeend',`
        <a class="btn-text" href="#">ГЕТЬ!</a>
        `)
        this.deleteButton.setAttribute('class', 'delete-btn')
        this.deleteButton.addEventListener('click', ()=>{
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                method:'DELETE'
            })
                .then(response => response.status)
                .then(data => {
                    if('200'){
                        this.card.remove()
                    }
                })
        })
    }
}




