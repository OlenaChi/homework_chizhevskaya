"use strict"

//Прототипне наслідування - це можливість на базі однієї моделі( об'єкта, шаблону) створювати подібні,
// не переписуючи кожен раз код. Є можливість додавання нових властивостей.
// super() - викликаємо тоді, коли створюємо новий об'єкт на основі батьківського.
// Властивості що копіюються з батьківського зазначаємо в функцію super(). Нові додаються після


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name(){
        return this.name;
    }
    set name(value){
        return this._name = value;
    }
    get age(){
        return this.age;
    }
    set age(value){

        return this._age = value;
    }
    get salary(){
        return this.salary;
    }
    set salary(value){
        return this._salary = value;
    }
}
class Programmer extends Employee {
        constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    };
    get salary(){
        return this._salary * 3;
    }

}
const junior = new Programmer('lena', 37, 500, 'en');
console.log(junior);

const senior = new Programmer('sam', 35, 1500, 'en');
console.log(senior);
console.log(junior.salary)
