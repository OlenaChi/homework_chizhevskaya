const books = [

    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: `70`
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const div = document.getElementById('root')
const booksList = document.createElement("ul")
div.prepend(booksList)

class Book {
    constructor(author, name, price) {
        if(!author){
            throw new NotInformationError('author')
        }
        if(!name){
            throw new NotInformationError('name')
        }
        if(!price){
            throw new NotInformationError('price')
        }

            this.author = author;
            this.name = name;
            this.price = price;
        }

    render() {
        booksList.insertAdjacentHTML('beforeend', `
    <li>
            <p>${this.author}</p>
            <h3>${this.name}</h3>
            <p>${this.price}</p>
     </li> `)
    }
}

class NotInformationError extends Error {
    constructor(value) {
        super();
        this.name = 'NotInformationError';
        this.message = `Didnt get any ${value}`;
    }
}

books.forEach((elem) => {
    try {
        new Book(elem.author, elem.name, elem.price).render()
    } catch (err){
        if(err.name ==='NotInformationError'){
            console.warn(err);
        } else {
            throw err
        }
    }
})




