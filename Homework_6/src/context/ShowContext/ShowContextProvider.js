import ShowContext from "./ShowContext";
import {useState} from "react";

const ShowContextProvider = ({children}) =>{
    const [currentShow, setCurrentShow] = useState(false)

    return (
                <ShowContext.Provider value ={{currentShow, setCurrentShow}}>
                    {children}
                </ShowContext.Provider>
    )
}
export default ShowContextProvider