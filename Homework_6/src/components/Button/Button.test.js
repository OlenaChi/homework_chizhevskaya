import Button from "./index";
import {render, fireEvent, screen} from "@testing-library/react";



describe("Button", ()=>{
    test("should Button render", ()=>{
        const {asFragment} = render( <Button className = "btn">BUTTON</Button>)
        expect(asFragment()).toMatchSnapshot()
    })
    it("should Button click", ()=>{
        const mockedFun = jest.fn()
        const{getByRole} = render( <Button onClick={mockedFun} >BUTTON</Button>)

        fireEvent.click(screen.getByRole("button"));
        expect(mockedFun).toHaveBeenCalledTimes(1);
    })
})