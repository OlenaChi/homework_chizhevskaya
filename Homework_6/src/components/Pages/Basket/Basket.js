import styles from "./Basket.module.scss"
import FormBasket from "../../FormBasket/FormBasket";
import ListBasket from "../../ListBasket/ListBasket";
import {useSelector} from "react-redux";
const Basket = () => {
const count = useSelector(store => store.counterAdd.counterAdd)
    return (
        <div className={styles.basket}>
            {count && <FormBasket/>}
            <ListBasket/>


        </div>
    )
}
export default Basket