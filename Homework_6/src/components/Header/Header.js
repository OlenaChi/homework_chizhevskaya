
import styles from './Header.module.scss'
import cartIcon from "../../svg/cart-outline.svg"
import starIcon from "../../svg/star.svg"
import starYellowIcon from "../../svg/star_yellow.svg"
import cottonFlower from "../../svg/flower.svg"
import PropTypes from 'prop-types'
import Navigation from "../Navigation/Navigation";
import {useSelector} from "react-redux";


const Header = () => {
const counterFavourite = useSelector(store=>store.counterFavourite.counterFavourite)
const counterAdd = useSelector(store=> store.counterAdd.counterAdd)
        return (
            <div className={styles.header}>
                    <h3 className={styles.title}>НАЙШИРШИЙ АСОРТИМЕНТ БАВОВНИ</h3>
              <Navigation style = {styles.icon}
                          cottonFlower = {
                              <div className={styles.image}>
                                  <img className={styles.imageIcon} src={cottonFlower} alt=""/>
                              </div>

                          }
                          favourite = {<div className={styles.image}>
                  <img className={styles.imageIcon} src={counterFavourite ? starYellowIcon : starIcon} alt=""/>
                  <p className={styles.iconCount}>{counterFavourite}</p>
                  </div>}
                          basket = {<div className={styles.image}>
                  <img className={styles.imageIcon} src={cartIcon} alt=""/>
                  <p className={styles.iconCount}>{counterAdd}</p>
                  </div>}
               />
            </div>
        )

}

// Header.propTypes = {
//     counterFavourite: PropTypes.number,
//     countAdd: PropTypes.number,
// }
// Header.defaultProps = {
//     countFavourite: 0,
//     countAdd: 0,
// }
export default Header

