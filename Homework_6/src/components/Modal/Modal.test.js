import Modal from "./Modal";
import {fireEvent, render, screen} from "@testing-library/react"
import {Provider, useDispatch} from "react-redux";
import {store} from "../../store";
import {setIsOpenModal} from "../../store/slices/modalSlices";

const MockedProvider=({children})=>(
    <Provider store={store}>
        <Component/>
    </Provider>
)


const Component = () => {
    const dispatch = useDispatch()
  return(<>
          <button onClick={()=>dispatch(setIsOpenModal(true))}>OPEN</button>
          <button onClick={()=>dispatch(setIsOpenModal(false))}>CLOSE</button>
          <Modal/>
  </>

  )
}
describe("Modal render", ()=>{
    test("should Modal match snapshot", ()=>{
        const {asFragment} = render(<MockedProvider/>);
        expect(asFragment()).toMatchSnapshot()
    })
})

describe("Modal open", ()=>{
    test("should Modal close", ()=>{
        render(<MockedProvider/>)
        fireEvent.click(screen.getByText("OPEN"))
        expect(screen.getByTestId("modal-root")).toBeInTheDocument()
    })
  })