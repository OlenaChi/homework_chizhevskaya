
import styles from './Card.module.scss'
import starIcon from '../../svg/star.svg'
import starYellowIcon from '../../svg/star_yellow.svg'
import PropTypes, {oneOfType} from 'prop-types'
import Button from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {setFavourite} from "../../store/slices/dataSlices";
// import {increment} from "../../store/slices/counterAddSlice";
import {incrementCounterAdd} from "../../store/slices/counterAddSlices";
import {incrementCounterFavourite} from "../../store/slices/counterFavouriteSlices";
import {addListBasket} from "../../store/slices/listBasketSlices";
import {setInfoModal, setIsOpenModal, setTypeOfModal} from "../../store/slices/modalSlices";
import {setFavouriteItem} from "../../util/function";


const Card = (
    {picture, name, color, price, id, isFavourite, typeOfStyle = false}
) => {
    const dispatch = useDispatch()

        return (

            <div className={typeOfStyle? styles.cardList : styles.card}>

                <button type = "button"  className={typeOfStyle?styles.likeBtnList :styles.likeBtn}
                        onClick={()=>{
                        dispatch(setFavouriteItem({picture, name, color, price, id, isFavourite}));

                }}
                >
                    <img className={styles.likeBtnIcon} src= {isFavourite ? starYellowIcon : starIcon} alt="star"/>
                </button>
                <div className={styles.image}>
                    <img className={styles.imageIcon} src={picture} alt={name}/>
                </div>
                <div className={typeOfStyle && styles.infoCardList}>
                    <div className={styles.cardInfo}>
                        <h3 className={styles.title}>{name}</h3>
                        <p className={styles.color}>код {id}</p>
                        <p className={styles.color}> колір {color}</p>
                        <p className={styles.price}>ціна: {price}</p>
                    </div >
                    <Button  style={typeOfStyle? styles.addBtnList : styles.addBtn} onClick={()=>{

                        dispatch(setIsOpenModal(true));
                        dispatch(setTypeOfModal(true));
                        dispatch(setInfoModal({picture, name, color, price, id,}))

                    }}

                             type = "button">Беру</Button>
                </div>

            </div>
        )

}

Card.propTypes = {
    picture: PropTypes.string,
    name: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string,PropTypes.number,]),
    id: PropTypes.oneOfType([PropTypes.string,PropTypes.number,]).isRequired,

    isFavourite: PropTypes.bool,

}
Card.defaultProps = {
    name: "Бавовна",
    picture: "https://novoeizdanie.com/wp-content/uploads/2022/05/ftohjlkwiaer0r8.jpg",
    color: "",
    isFavourite: false,
    price: "free",
    }
export default Card