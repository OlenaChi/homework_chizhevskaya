import styles from './Main.module.scss'
import Button from "../Button/Button";
import list from "../../svg/list.svg"
import grid from "../../svg/grid.svg"
import Card from "../Card/Card";
// import PropTypes from 'prop-types';
import {useContext} from "react";
import ShowContext from "../../context/ShowContext/ShowContext";

import {shallowEqual, useSelector} from "react-redux";
import {current} from "immer";
// import ShowContextProvider from "../../context/ShowContext/ShowContextProvider";

const Main =()=>{
    const data = useSelector(store => store.data.data, shallowEqual)
    const {currentShow, setCurrentShow} = useContext(ShowContext)
const mainStyleGrid = `${styles.main} ${currentShow && styles.list}`

        return (
            // <ShowContextProvider>
            <div className={mainStyleGrid}>
                <Button style={styles.btnChange} onClick={()=>setCurrentShow(prev=>!prev)}>
                    <div className={styles.img}>
                        <img className={styles.imgIcon} src={currentShow? grid : list } alt=""/>
                    </div>

                </Button>
                {data.map(({picture, name, color, price, id, isFavourite}) => <Card typeOfStyle={currentShow}
                 key = {id} picture = {picture} name = {name} color = {color} price = {price} id = {id} isFavourite={isFavourite}/>)}
               {/*<div  className={styles.bgi}/>*/}
            </div>
            // </ShowContextProvider>

        )

}

// Main.propTypes = {
//     data: PropTypes.array.isRequired,
//     incrementFavourite: PropTypes.func,
//     toggleModal: PropTypes.func.isRequired,
//
//
// }
// Main.defaultProps = {
//     incrementFavourite: () =>{},
//     setTitleModal: () =>{},
//
// }
export default Main