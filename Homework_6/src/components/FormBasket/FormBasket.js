import {Formik, Form, Field, ErrorMessage} from "formik";
import React from "react";
import styles from "./FormBasket.module.scss"
import * as yup from "yup"
// import Button from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {submitOrder} from "../../util/function";

const FormBasket =() =>{
    const dispatch = useDispatch()
    const order = useSelector(store => store.listBasket.listBasket)
const initialValues = {
    name:"",
    lastName:"",
    age:"",
    address:"",
    phone:""


}
const validationSchema = yup.object().shape({
    name: yup.string()
        .min(3, "Мінімум три символи")
        .max(20, "Максимум 20 символів")
        .required("Заповни це поле, будь ласка")
        .matches(/[A-Za-zА-Яа-я ]/g,"Нормально напиши, будь ласка"),

    lastName: yup.string()
        .min(2, "Мінімум два символи")
        .max(25, "Максимум 20 символів")
        .required("Заповни це поле, будь ласка"),

    age: yup.number()
        .min(18, "Треба ще трошки подорослішати")
        .max(110, "Щось забагато, перевір дані")
        .required("Заповни це поле, будь ласка"),

    address: yup.string()
        .required("Заповни це поле, будь ласка"),

    phone: yup.number()
        .required("Заповни це поле, будь ласка"),

    // age: yup.number().min(18),
    // email: yup.string().email("Некорректна пошта")

})
    return(
        // <form action="">
        //     <input type="text"/>
        //     <input type="text"/>
        //     <input type="text"/>
        // </form>
      <Formik
          initialValues = {initialValues}
          validationSchema = {validationSchema}
          onSubmit = {(values, {resetForm})=>{
          resetForm()
          }}
      >
          {/*{({isValid})*/}

          {(props)   => {
console.log(props)
              return (
                  <Form className={styles.form}>
                      <p>Заповни форму</p>
                      <Field
                          type="text"
                          name="name"
                          placeholder="Ім'я"
                      />
                      <ErrorMessage name = "name">{msg=> <span className={styles.error}>{msg}</span>}</ErrorMessage>

                      <Field
                          type="text"
                          name="lastName"
                          placeholder="Прізвище"
                      />
                      <ErrorMessage name = "lastName">{msg=> <span className={styles.error}>{msg}</span>}</ErrorMessage>

                      <Field
                          type="text"
                          name="age"
                          placeholder="Вік"
                      />
                      <ErrorMessage name = "age">{msg=> <span className={styles.error}>{msg}</span>}</ErrorMessage>

                      <Field
                          type="text"
                          name="address"
                          placeholder="Адреса"
                      />
                      <ErrorMessage name = "address">{msg=> <span className={styles.error}>{msg}</span>}</ErrorMessage>

                      <Field
                          type="text"
                          name="phone"
                          placeholder="Контактний номер"
                      />
                      <ErrorMessage name = "phone">{msg=> <span className={styles.error}>{msg}</span>}</ErrorMessage>

                      <button
                          disabled = {!props.isValid}
                          type="submit"
                          className={styles.btnSubmit}
                          // style={styles.btnSubmit}
                              onClick={()=>{dispatch(submitOrder(props.values, order))}}
                      >ВІДПРАВИТИ</button>
                  </Form>
              )
          }}


      </Formik>
    )
}
export default FormBasket