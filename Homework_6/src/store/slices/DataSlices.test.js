import dataSlices from "./dataSlices";
import {addData, setFavourite, setNotFavourite} from "./dataSlices"
const initialState={
    data:[]
}

const dataPayload = [{ id:1, isFavourite:false }, { id:2, isFavourite:false }]
const initialStateData = {
    data: [{ id:1, isFavourite:false }, { id:2, isFavourite:false }]
}

const initialStateDataFavourite = {
    data: [{ id:1, isFavourite:true }, { id:2, isFavourite:false }]
}


describe("data", ()=>{
    test("should return the initial state", ()=>{
        expect(dataSlices(undefined, {type: undefined})).toEqual(initialState)
    })
    test("should set data", ()=>{
        expect(dataSlices(initialState, {type: addData, payload: dataPayload})).toEqual(initialStateData)
    })
    test("should set favourite", ()=>{
        expect(dataSlices(initialStateData, {type: setFavourite, payload: {id:1}})).toEqual(initialStateDataFavourite)
    })
    test("should set notFavourite", ()=>{
        expect(dataSlices(initialStateDataFavourite, {type: setNotFavourite, payload: {id:1}})).toEqual(initialStateData)
    })
})