import ModalSlices from "./modalSlices";
import {setIsOpenModal, setTypeOfModal, setInfoModal} from "./modalSlices"
import modalSlices from "./modalSlices";
const info={
    id:1
}
const initialState = {
    isOpenModal:false,
    typeOfModal:false,
    infoModal:{}
}
const initialStateIsOpen = {
    isOpenModal:true,
    typeOfModal:false,
    infoModal:{}
}
const initialStateIsOpenTypeModal = {
    isOpenModal:true,
    typeOfModal:true,
    infoModal:{}
}
const initialStateIsOpenTypeModalInfo = {
    isOpenModal:true,
    typeOfModal:true,
    infoModal:{id:1}
}

describe("Modal work", ()=>{
    test("should isOpen true", ()=>{
      expect(modalSlices(initialState,{type:setIsOpenModal, payload: true})).toEqual(initialStateIsOpen)
    })
    test("should change typeOfModal", ()=>{
        expect(modalSlices(initialStateIsOpen,{type:setTypeOfModal, payload: true})).toEqual(initialStateIsOpenTypeModal)
    })
    test("should set info", ()=>{
        expect(modalSlices(initialStateIsOpenTypeModal,{type:setInfoModal, payload: info})).toEqual(initialStateIsOpenTypeModalInfo )
    })
})

