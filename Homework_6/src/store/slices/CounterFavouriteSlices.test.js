import counterFavouriteSlices from "./counterFavouriteSlices";
import {incrementCounterFavourite, setCounterFavourite, decrementCounterFavourite} from "./counterFavouriteSlices";

const initialState = {
    counterFavourite:0
}
describe('counterFavourite works', ()=>{
    test("should return the initial state", ()=>{
        expect(counterFavouriteSlices(undefined, {type:undefined})).toEqual(initialState)
    })

    test("should set the state", ()=>{
        expect(counterFavouriteSlices(initialState, {type:setCounterFavourite, payload:10})).toEqual({
            counterFavourite:10
        })
    })

    test("should increment counter", ()=>{
        expect(counterFavouriteSlices(initialState, {type:incrementCounterFavourite})).toEqual({
            counterFavourite:1
        })
    })

    test("should decrement counter", ()=>{
        expect(counterFavouriteSlices(initialState, {type:decrementCounterFavourite})).toEqual({
            counterFavourite:-1
        })
    })

})