import {createSlice} from "@reduxjs/toolkit";
import {getListBasket} from "../../api/getData";

const listBasketSlices = createSlice({
    name:"listBasket",
    initialState:{
        listBasket:[]
    },
    reducers:{
        setListBasket:(state, action)=>{
            state.listBasket = action.payload;

        },
        addListBasket:(state,action) =>{
           console.log(action.payload)
           const index = state.listBasket.findIndex(el=> el.id === action.payload.id)
            console.log(index)
            if(index === -1){
                state.listBasket.push({...action.payload, countItem:1})
                localStorage.setItem('listBasket', JSON.stringify(state.listBasket))
            }else{
                state.listBasket[index].countItem+=1
                localStorage.setItem('listBasket', JSON.stringify(state.listBasket))
            }
        },
        decrementListItem:(state, action) =>{
            const index = state.listBasket.findIndex(el=> el.id === action.payload.id)
            state.listBasket[index].countItem-=1
            localStorage.setItem('listBasket', JSON.stringify(state.listBasket))
        },
        delListBasketItem:(state, action)=>{
            const index = state.listBasket.findIndex(el=> el.id === action.payload.id)
            state.listBasket.splice(index, 1)
            localStorage.setItem('listBasket', JSON.stringify(state.listBasket))

        }
    }
})



export const fetchListBasket = ()=> async(dispatch) =>{
    const data = await getListBasket();
    data? dispatch(setListBasket(data)) : dispatch(setListBasket([]))
    ;
}

export const {setListBasket, addListBasket, delListBasketItem, decrementListItem} = listBasketSlices.actions
export default listBasketSlices.reducer