import listBasketSlices from "./listBasketSlices";
import {setListBasket, addListBasket, delListBasketItem, decrementListItem} from "./listBasketSlices";

const initialState = {
    listBasket: []
}
const newItem = {
    id: 3,
    countItem:1
}
const listBasket = [{id:1, countItem:1}, {id:2, countItem:2}]
const initialStateData={
    listBasket:[{id:1, countItem:1}, {id:2, countItem:2}]
}
const initialStateWidthNewItem = {
    listBasket:[{id:1, countItem:1}, {id:2, countItem:2},{id:3, countItem: 1}]
}
const initialStateWidthTopItemsCount={
    listBasket:[{id:1, countItem:1}, {id:2, countItem:2},{id:3, countItem: 2}]
}

describe("listBasket", ()=>{
    test("should return initial state", ()=>{
        expect(listBasketSlices(undefined,{type:undefined})).toEqual(initialState)
    })
    test("should set the state", ()=>{
        expect(listBasketSlices(initialState,{type:setListBasket, payload:listBasket})).toEqual(initialStateData)
    })
    test("should add new item to listBasket", ()=>{
        expect(listBasketSlices(initialStateData,{type:addListBasket, payload:newItem})).toEqual(initialStateWidthNewItem)
    })
    test("should increment items count", ()=>{
        expect(listBasketSlices(initialStateWidthNewItem,{type:addListBasket, payload:newItem})).toEqual(initialStateWidthTopItemsCount)
    })
    test("should delete items", ()=>{
        expect(listBasketSlices(initialStateWidthNewItem,{type:delListBasketItem, payload:newItem})).toEqual(initialStateData)
    })
    test("should decrement counter items", ()=>{
        expect(listBasketSlices(initialStateWidthTopItemsCount,{type:decrementListItem, payload:newItem})).toEqual(initialStateWidthNewItem)
    })
})