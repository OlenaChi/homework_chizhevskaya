import counterAddSlices from "./counterAddSlices";
import {setCounterAdd, incrementCounterAdd, decrementCounterAdd} from "./counterAddSlices";

const initialState = {
    counterAdd:0
}
describe('counterAdd works', ()=>{
    test("should return the initial state", ()=>{
        expect(counterAddSlices(undefined, {type:undefined})).toEqual(initialState)
    })

    test("should set the state", ()=>{
        expect(counterAddSlices(initialState, {type:setCounterAdd, payload:10})).toEqual({
                counterAdd:10
            })
    })

    test("should increment counter", ()=>{
        expect(counterAddSlices(initialState, {type:incrementCounterAdd})).toEqual({
            counterAdd:1
        })
    })

    test("should decrement counter", ()=>{
        expect(counterAddSlices(initialState, {type:decrementCounterAdd, payload:3})).toEqual({
            counterAdd:-3
        })
    })

})