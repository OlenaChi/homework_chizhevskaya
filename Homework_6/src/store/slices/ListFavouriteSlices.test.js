import listFavouriteSlices from "./listFavouriteSlices";
import {setListFavourite, deleteListFavouriteItem, addListFavouriteItem} from "./listFavouriteSlices"
import listFavourite from "../../components/Pages/ListFavourite/ListFavourite";

const initialState= {
    listFavourite: []
}

const item = {
    id:3,
    isFavourite:true
}
const listFavouriteState = [{id:1, isFavourite: true},{id:2, isFavourite: true}]
const initialStateSet={
    listFavourite: [{id:1, isFavourite: true},{id:2, isFavourite: true}]
}
const initialStateWidthNewItem={
    listFavourite: [{id:1, isFavourite: true},{id:2, isFavourite: true},{id:3, isFavourite: true}]
}


describe("ListFavourite testing", ()=>{
    test("should return initial value", ()=>{
        expect(listFavouriteSlices(undefined,{type:undefined})).toEqual(initialState)
    })
    test("should set the state", ()=>{
        expect(listFavouriteSlices(initialState,{type:setListFavourite, payload: listFavouriteState})).toEqual(initialStateSet)
    })
    test("should add item", ()=>{
        expect(listFavouriteSlices(initialStateSet,{type:addListFavouriteItem, payload: item})).toEqual(initialStateWidthNewItem)
    })
    test("should delete item", ()=>{
        expect(listFavouriteSlices(initialStateWidthNewItem,{type:deleteListFavouriteItem, payload: item})).toEqual(initialStateSet)
    })
})