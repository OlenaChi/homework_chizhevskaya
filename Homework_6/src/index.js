import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {store} from "./store";
import ShowContextProvider from "./context/ShowContext/ShowContextProvider";
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
<React.StrictMode>
    <Provider store={store}>
        <BrowserRouter>
            <ShowContextProvider>
    <App />
                </ShowContextProvider>
         </BrowserRouter>
        </Provider>
</React.StrictMode>


);

