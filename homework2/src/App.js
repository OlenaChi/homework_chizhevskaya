
import './App.css';
import { Component } from "react";
import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import Modal from "./components/Modal/Modal";




class App extends Component{
 state = {
     countFavourite: 0,
     countAdd:0,
     isOpenModal: false,
     data:[],
     titleModal: "",
 };





incrementFavourite = (isFavourite) => {
if(!isFavourite){this.setState((prev)=>({

        countFavourite: prev.countFavourite + 1  }),
    () =>{
        localStorage.setItem('countFavourite', JSON.stringify(this.state.countFavourite))
    }
)} else {this.setState((prev)=>({

        countFavourite: prev.countFavourite - 1  }),
    () =>{
        localStorage.setItem('countFavourite', JSON.stringify(this.state.countFavourite))
    }
)}

    }

    clickFavourite = (id, isFavourite) => {
             this.setState((prev)=>{
               const arrayData = [...prev.data]
                console.log(arrayData)
                const index = arrayData.findIndex(el => el.id === id)
                console.log(this.state.data[index])
                  arrayData[index].isFavourite = !isFavourite
                console.log(this.state.data[index])
                localStorage.setItem('data', JSON.stringify(arrayData))
                return arrayData
            })
       }

    incrementAdd = () => {
        this.setState((prev)=>({ countAdd: prev.countAdd + 1  }),
            ()=>{
                localStorage.setItem('countAdd', JSON.stringify(this.state.countAdd))
            })

    }

    toggleModal= (value) => {
        this.setState({isOpenModal: value})
    }
setTitleModal = (value) => {
        this.setState({titleModal: value})
}

    async componentDidMount(){
        // if(this.state.countFavourite > 0)
     if(localStorage.getItem('countFavourite'))
        { if(localStorage.getItem('countAdd')){
            const countAdd = localStorage.getItem('countAdd')
            this.setState({countAdd: JSON.parse(countAdd)})}

        const countFavourite = localStorage.getItem('countFavourite')
         const arrayData = localStorage.getItem('data')

         this.setState({countFavourite: JSON.parse(countFavourite)})
         this.setState({data: JSON.parse(arrayData)})

     } else {const array = await fetch(`./data.json`).then(res=>res.json())
                  this.setState({data: array})
         if(localStorage.getItem('countAdd')){
             const countAdd = localStorage.getItem('countAdd')
             this.setState({countAdd: JSON.parse(countAdd)})}

         }

 }
    async componentDidUpdate(){
        if(this.state.countFavourite > 0)

        {

            const arrayData = localStorage.getItem('data')

            this.setState({data: JSON.parse(arrayData)})


        } else {const array = await fetch(`./data.json`).then(res=>res.json())
            this.setState({data: array})

        }
    }
 render(){
const {countFavourite, countAdd, isOpenModal, data, titleModal} = this.state


     return (

         <div className="App">
             <Header countFavourite = {countFavourite} countAdd = {countAdd}  />

             <Main data = {data} clickFavourite={this.clickFavourite} incrementFavourite={this.incrementFavourite} toggleModal={this.toggleModal} setTitleModal={this.setTitleModal} />
             <Modal incrementAdd={this.incrementAdd} isOpenModal={isOpenModal} toggleModal={this.toggleModal} titleModal={titleModal}/>
         </div>


     )
 }


}

export default App;
