import styles from './Main.module.scss'
import { PureComponent } from "react";
import Card from "../Card/Card";
import PropTypes from 'prop-types'

class Main extends PureComponent {

    render(){
const {data, incrementFavourite, toggleModal, setTitleModal, clickFavourite} = this.props
        console.log(data)
        return (
            <div className={styles.main}>
                {data.map(({picture, name, color, price, id, isFavourite}) => <Card clickFavourite = {clickFavourite} setTitleModal= {setTitleModal} toggleModal={toggleModal} incrementFavourite = {incrementFavourite} key = {id} picture = {picture} name = {name} color = {color} price = {price} id = {id} isFavourite = {isFavourite}/>)}
               <div  className={styles.bgi}/>
            </div>

        )
    }
}

Main.propTypes = {
    data: PropTypes.array.isRequired,
    incrementFavourite: PropTypes.func,
    toggleModal: PropTypes.func.isRequired,
    setTitleModal: PropTypes.func,
    clickFavourite: PropTypes.func,
}
Main.defaultProps = {
    incrementFavourite: () =>{},
    setTitleModal: () =>{},
    clickFavourite: () =>{},
}
export default Main