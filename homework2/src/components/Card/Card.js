import {PureComponent} from "react";
import styles from './Card.module.scss'
import starIcon from '../../svg/star.svg'
import starYellowIcon from '../../svg/star_yellow.svg'
import PropTypes, {oneOfType} from 'prop-types'


class Card extends PureComponent {

    render(){
        const {picture, name, color, price, id, incrementFavourite, isFavourite, toggleModal, setTitleModal, clickFavourite} = this.props
        return (
            <div className={styles.card}>

                <button type = "button"  className={styles.likeBtn} onClick={()=>{incrementFavourite(isFavourite) ; clickFavourite(id,isFavourite)}}>
                    <img src= {isFavourite ? starYellowIcon : starIcon} alt="star"/>
                </button>
                <div className={styles.image}>
                    <img src={picture} alt={name}/>
                </div>
                <div className={styles.cardInfo}>
                    <h3 className={styles.title}>{name}</h3>
                    <p className={styles.color}>код {id}</p>
                    <p className={styles.color}> колір {color}</p>
                    <p className={styles.price}>ціна: {price}</p>
                </div>
                <button  className={styles.addBtn} onClick={()=> {
                    toggleModal(true);
                    setTitleModal(name)
                }} type = "button">Беру</button>
            </div>
        )
    }
}

Card.propTypes = {
    picture: PropTypes.string,
    name: PropTypes.string.isRequired,
    color: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string,PropTypes.number,]).isRequired,
    id: PropTypes.oneOfType([PropTypes.string,PropTypes.number,]),
    incrementFavourite: PropTypes.func,
    isFavourite: PropTypes.bool,
    toggleModal: PropTypes.func.isRequired,
    setTitleModal: PropTypes.func,
    clickFavourite: PropTypes.func,
}
Card.defaultProps = {
    picture: "",
    color: "",
    incrementFavourite: ()=>{},
    isFavourite: false,
    setTitleModal: ()=>{},
    clickFavourite: ()=>{},
}
export default Card