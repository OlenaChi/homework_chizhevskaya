import { PureComponent } from "react";
import styles from './Header.module.scss'
import cartIcon from "../../svg/cart-outline.svg"
import starIcon from "../../svg/star.svg"
import starYellowIcon from "../../svg/star_yellow.svg"
import PropTypes from 'prop-types'

class Header extends PureComponent {

    render(){
        const {countFavourite, countAdd} = this.props
        return (
            <div className={styles.header}>
                    <h3 className={styles.title}>НАЙШИРШИЙ АСОРТИМЕНТ БАВОВНИ</h3>
<ul>
    <li className={styles.icon}>
        <img src={countFavourite ? starYellowIcon : starIcon} alt=""/>
        <p className={styles.iconCount}>{countFavourite}</p>
    </li>
    <li className={styles.icon}>
        <img  src={cartIcon} alt=""/>
        <p className={styles.iconCount}>{countAdd}</p>
    </li>

</ul>

            </div>
        )
    }
}

Header.propTypes = {
    countFavourite: PropTypes.number,
    countAdd: PropTypes.number,
}
Header.defaultProps = {
    countFavourite: 0,
    countAdd: 0,
}
export default Header