import styles from './Button.module.scss';
import { PureComponent }from "react";
import PropTypes from 'prop-types'
class Button extends PureComponent {

    render() {
        const {children, onClick} = this.props;
        return (
              <button className = {styles.btn} onClick={onClick} type = "button">{children}</button>
        )
    }
}
Button.propTypes = {
    children: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
}

export default Button;