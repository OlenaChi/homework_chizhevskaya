import styles from './Modal.module.scss';
import { PureComponent }from "react";
import PropTypes from 'prop-types'
import Button from "../Button/Button";

class Modal extends PureComponent {

    render() {
        const { isOpenModal, toggleModal, titleModal, incrementAdd} = this.props;
if(!isOpenModal){
    return null
}
        return (
            <>
                <div className={styles.modal}>
                    <div  className={styles.modalBackground} onClick={()=>toggleModal(false)}/>

                    <div className={styles.modalMainContainer} >
                        <button className = {styles.btnX} onClick={()=>toggleModal(false)}>X</button>
                        <div className={styles.modalContentWrapper}>
                            <header className={styles.modalHeader}>
                                Подобається {titleModal} ?</header>
                            <div className={styles.modalBody}>Треба брати</div>
                        </div>
                        <div className={styles.modalButtonWrapper}>
                            <Button onClick={()=>toggleModal(false)}> Ще подумаю</Button>
                            <Button onClick={() => {
                                incrementAdd();
                                toggleModal(false);
                            }}> Авжеж беру</Button>

                        </div>
                    </div>
                </div>


            </>
        )
    }
}
Modal.propTypes = {
    isOpenModal: PropTypes.bool.isRequired,
    toggleModal: PropTypes.func.isRequired,
    titleModal: PropTypes.string,
    incrementAdd: PropTypes.func.isRequired
}
Modal.propTypes = {
    titleModal: "",

}
export default Modal;