
import './App.css';
import { useEffect, useState} from "react";
import Header from "./components/Header/Header";
import AppRoutes from "./AppRoutes";
import Modal from "./components/Modal/Modal";
import { BrowserRouter } from 'react-router-dom';



const App = () => {
  const [countFavourite, setCountFavourite] = useState(0);
  const [countAdd, setCountAdd] = useState(0);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [modalType, setModalType] = useState(true);
  const [data, setData] = useState([]);
  const [infoModal, setInfoModal] = useState({});
  const [listFavourite, setListFavourite] = useState([]);
  const [listBasket, setListBasket] = useState(JSON.parse(localStorage.getItem('basket')) || []);
console.log(listBasket)

  const incrementFavourite = (picture, name, color, price, id, isFavourite) => {
    setData((prev)=> {
      const arrayData = [...prev]
      const index = arrayData.findIndex(el => el.id === id)
      arrayData[index].isFavourite = !isFavourite

      localStorage.setItem('data', JSON.stringify(arrayData))
      return arrayData
    })
    if(!isFavourite){
        setCountFavourite((prev)=> {
            const newCount = prev + 1
            localStorage.setItem('countFavourite', JSON.stringify(newCount))
            return newCount
        })
        setListFavourite((prev)=>{
            const arrayFavourite = [...prev]
            const cardFavourite = {
                 id,
                 picture,
                 name,
                 isFavourite: !isFavourite,
                 price,
                 color
            }
            arrayFavourite.push(cardFavourite)
            localStorage.setItem('listFavourite', JSON.stringify(arrayFavourite))
            return arrayFavourite
        })
    } else {
        setCountFavourite((prev) => {
            const newCount = prev - 1
            localStorage.setItem('countFavourite', JSON.stringify(newCount))
            return newCount
        })
        setListFavourite((prev)=>{

            const arrayFavourite = [...prev]
            const index = arrayFavourite.findIndex(el => el.id === id)
            arrayFavourite.splice(index, 1)
            localStorage.setItem('listFavourite', JSON.stringify(arrayFavourite))
            return arrayFavourite
        })

    }

  };


    const incrementAdd  = (infoItem ) => {
        setCountAdd((prev)=>{
            const count = prev+1
            localStorage.setItem('countAdd', JSON.stringify(count))
            return count
        })

        setListBasket((prev)=>{
            const arrayBasket = [...prev]
            const index = arrayBasket.findIndex(el =>el.id === infoItem.id)
            if(index === -1) {
                arrayBasket.push({...infoItem, countItem:1})
                localStorage.setItem('basket', JSON.stringify(arrayBasket))
                return arrayBasket
            }else{
               //Это Ваша версия, к сожалению, она тоже некорректно работает. Можно проверить выбрав несколько единиц товара в корзину более чем по одному разу
               //  arrayBasket[index].countItem = countAdd + 1
                //это моя версия. она так и ходит по кругу
              // arrayBasket[index].countItem += 1
 //               ВОТ ТЕПЕРЬ РАБОТАЕТ
 const i = arrayBasket[index].countItem
                console.log(i)
                const ii = i+1
                console.log(ii)
                arrayBasket.splice(index,1,{...infoItem, countItem:ii})

                localStorage.setItem('basket', JSON.stringify(arrayBasket))
                return arrayBasket
            }

        })
    };




    const decrementAdd = (id,countItem= 0) => {
        if(countItem > 1){
            const arrayBasket = [...JSON.parse(localStorage.getItem('basket'))]
            const index = arrayBasket.findIndex(el => el.id === id)
            arrayBasket[index].countItem -=1
            localStorage.setItem('basket', JSON.stringify(arrayBasket))
            setListBasket(arrayBasket)
            console.log(listBasket)

            setCountAdd((prev) => {
                const count = prev-1
                localStorage.setItem('countAdd', JSON.stringify(count))
                return count
            })


        }
        if (countItem === 1){
            toggleModal(true)
            setModalType(false)
        }
        if (countItem === 0){
            const arrayBasket = [...JSON.parse(localStorage.getItem('basket'))]
            const index = arrayBasket.findIndex(el => el.id === id)
            arrayBasket.splice(index, 1)
            localStorage.setItem('basket', JSON.stringify(arrayBasket))
            setCountAdd((prev) => {
                const count = prev-1
                localStorage.setItem('countAdd', JSON.stringify(count))
                return count
            })
            setListBasket(arrayBasket)
        }


    }


  const toggleModal = (value) => {
    setIsOpenModal(value)

  };


  useEffect(()=>{
    const favour = localStorage.getItem('countFavourite')
       if(favour){
             setCountFavourite( JSON.parse(favour))
             const arrayData = localStorage.getItem('data')
             setData(JSON.parse(arrayData))
             const listFavour = localStorage.getItem('listFavourite')
             setListFavourite(JSON.parse(listFavour))
        }else{(async ()=>{
             const array = await fetch(`./data.json`).then(res=>res.json())
             setData( array)
        })()
        }
    const addBasket = localStorage.getItem('countAdd')
       if(addBasket){
             setCountAdd( JSON.parse(addBasket))
       }
    // const basket = localStorage.getItem('basket')
    //    if(!!basket){
    //        console.log(typeof (basket))
    //       setListBasket(prev => JSON.parse(basket))
    //
    //    }
   },[])



  return (
<BrowserRouter>
      <div className="App">
        <Header countFavourite = {countFavourite} countAdd = {countAdd}  />
        <AppRoutes data = {data}  incrementFavourite={incrementFavourite} toggleModal={toggleModal}
                   listFavourite={listFavourite} setModalType={setModalType} setInfoModal={setInfoModal} listBasket={listBasket} decrementAdd={decrementAdd} incrementAdd={incrementAdd} />

        <Modal decrementAdd={decrementAdd} modalType={modalType}  incrementAdd={incrementAdd} isOpenModal={isOpenModal} toggleModal={toggleModal}  infoModal={infoModal}/>
      </div>
 </BrowserRouter>

  )



}

export default App;
