import { Routes, Route } from 'react-router-dom'
import Main from "./components/Main/Main";
import PropTypes from 'prop-types'
import ListFavourite from "./components/Pages/ListFavourite/ListFavourite";
import ListBasket from "./components/Pages/ListBasket/ListBasket";
const AppRoutes =({data, incrementFavourite, toggleModal, setModalType, listFavourite, setInfoModal, listBasket, decrementAdd, incrementAdd }) =>{

    return(
        <Routes>
            <Route path = "/" element = {
               <>
                <Main data = {data}
                       incrementFavourite={incrementFavourite} toggleModal={toggleModal} setModalType={setModalType} setInfoModal={setInfoModal}
                />
               </> }
               />
            <Route path = "/favourite" element = {
                <>
                    <ListFavourite
                listFavourite = {listFavourite}
                incrementFavourite={incrementFavourite} setInfoModal={setInfoModal} toggleModal={toggleModal} setModalType={setModalType}
                    />
                </>}/>
            <Route path = "/basket" element = {<ListBasket setInfoModal={setInfoModal} setModalType={setModalType} incrementAdd={incrementAdd} listBasket={listBasket} decrementAdd={decrementAdd} toggleModal={toggleModal}/>}/>
        </Routes>
    )
}
AppRoutes.propTypes = {
    data: PropTypes.array.isRequired,
    incrementFavourite: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired,
    setInfoModal:PropTypes.func.isRequired,
    clickFavourite: PropTypes.func,
}

export default AppRoutes