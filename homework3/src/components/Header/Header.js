
import styles from './Header.module.scss'
import cartIcon from "../../svg/cart-outline.svg"
import starIcon from "../../svg/star.svg"
import starYellowIcon from "../../svg/star_yellow.svg"
import cottonFlower from "../../svg/flower.svg"
import PropTypes from 'prop-types'
import Navigation from "../Navigation/Navigation";


const Header = ({countFavourite, countAdd}) => {


        return (
            <div className={styles.header}>
                    <h3 className={styles.title}>НАЙШИРШИЙ АСОРТИМЕНТ БАВОВНИ</h3>
              <Navigation style = {styles.icon}
                          cottonFlower = {
                              <div className={styles.image}>
                                  <img className={styles.imageIcon} src={cottonFlower} alt=""/>
                              </div>

                          }
                          favourite = {<div className={styles.image}>
                  <img className={styles.imageIcon} src={countFavourite ? starYellowIcon : starIcon} alt=""/>
                  <p className={styles.iconCount}>{countFavourite}</p>
                  </div>}
                          basket = {<div className={styles.image}>
                  <img className={styles.imageIcon} src={cartIcon} alt=""/>
                  <p className={styles.iconCount}>{countAdd}</p>
                  </div>}
               />
            </div>
        )

}

Header.propTypes = {
    countFavourite: PropTypes.number,
    countAdd: PropTypes.number,
}
Header.defaultProps = {
    countFavourite: 0,
    countAdd: 0,
}
export default Header

