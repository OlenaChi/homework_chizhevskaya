
import styles from './Card.module.scss'
import starIcon from '../../svg/star.svg'
import starYellowIcon from '../../svg/star_yellow.svg'
import PropTypes, {oneOfType} from 'prop-types'
import Button from "../Button/Button";


const Card = ({picture, name, color, price, id, isFavourite, incrementFavourite, toggleModal, setInfoModal, setModalType }) => {


        return (

            <div className={styles.card}>

                <button type = "button"  className={styles.likeBtn} onClick={()=>{incrementFavourite(picture, name, color, price, id, isFavourite)}}>
                    <img className={styles.likeBtnIcon} src= {isFavourite ? starYellowIcon : starIcon} alt="star"/>
                </button>
                <div className={styles.image}>
                    <img className={styles.imageIcon} src={picture} alt={name}/>
                </div>
                <div className={styles.cardInfo}>
                    <h3 className={styles.title}>{name}</h3>
                    <p className={styles.color}>код {id}</p>
                    <p className={styles.color}> колір {color}</p>
                    <p className={styles.price}>ціна: {price}</p>
                </div>
                <Button  style={styles.addBtn} onClick={()=> {
                    toggleModal(true);
                    setModalType(true);
                    setInfoModal({picture, name, color, price, id});
                }} type = "button">Беру</Button>
            </div>
        )

}

Card.propTypes = {
    picture: PropTypes.string,
    name: PropTypes.string.isRequired,
    color: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string,PropTypes.number,]).isRequired,
    id: PropTypes.oneOfType([PropTypes.string,PropTypes.number,]),
    incrementFavourite: PropTypes.func.isRequired,
    isFavourite: PropTypes.bool.isRequired,
    toggleModal: PropTypes.func.isRequired,
    setInfoModal: PropTypes.func.isRequired,
    setModalType: PropTypes.func.isRequired,
}
Card.defaultProps = {
    picture: "",
    color: "",
    }
export default Card