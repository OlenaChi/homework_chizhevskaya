import styles from './ListFavourite.module.scss'

import PropTypes from 'prop-types'
import Card from  "../../Card/Card";

const ListFavourite = ({listFavourite, incrementFavourite, toggleModal, setInfoModal, setModalType}) => {

    return (<>
            <h2>Лист вподобань</h2>
        <div className={styles.containerFavourite}>

            {listFavourite.map(({picture, name, color, price, id, isFavourite})=> <Card  setInfoModal={setInfoModal} setModalType={setModalType} toggleModal={toggleModal} incrementFavourite = {incrementFavourite} key = {id} picture = {picture} name = {name} color = {color} price = {price} id = {id} isFavourite = {isFavourite}/>)}

        </div>
            <div className={styles.bgi} />
        </>
    )
}
ListFavourite.propTypes = {
    listFavourite:PropTypes.array.isRequired,
    incrementFavourite: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired,
    setInfoModal: PropTypes.func.isRequired,
    setModalType: PropTypes.func.isRequired,
}
export default ListFavourite