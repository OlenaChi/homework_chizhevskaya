import BasketItem from "../../BasketItem/BasketItem";
import styles from "./ListBasket.module.scss";
import PropTypes from 'prop-types';

const ListBasket = ({listBasket, decrementAdd, toggleModal,incrementAdd, setInfoModal, setModalType}) => {
    return (<>
        <div className={styles.wrapper}>
            {listBasket.map(({picture, name, color, price, id, countItem}) => <BasketItem setModalType={setModalType} setInfoModal={setInfoModal} incrementAdd={incrementAdd} toggleModal={toggleModal} decrementAdd={decrementAdd} picture={picture} name = {name} color = {color} price = {price} id = {id} countItem = {countItem} key = {id}/>)}
        </div>
            <div  className={styles.bgi}/>
        </>
    )
}

ListBasket.propTypes = {
    listBasket: PropTypes.array.isRequired,
    decrementAdd: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired,
    incrementAdd: PropTypes.func.isRequired,
    setInfoModal: PropTypes.func.isRequired,
    setModalType: PropTypes.func.isRequired,
}
export default ListBasket