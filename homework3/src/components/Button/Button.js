

import PropTypes from 'prop-types'
const Button = ({children, onClick, style}) => {



        return (
              <button className = {`${style}`} onClick={onClick} type = "button">{children}</button>
        )

}
Button.propTypes = {

    onClick: PropTypes.func.isRequired,
}

export default Button;