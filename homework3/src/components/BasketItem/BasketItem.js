import styles from './BasketItem.module.scss'
import Button from "../Button/Button";
import crossIcon from "../../svg/cross.svg";
import PropTypes from 'prop-types';


const BasketItem = ({picture, name, color, price, id, countItem, decrementAdd, toggleModal, incrementAdd, setInfoModal, setModalType}) => {
const info = {
    picture, name, color, price, id, countItem
}
    return (
                  <div className = {styles.item}>
                      <Button style = {styles.btnDel} onClick={()=>{toggleModal(true); setModalType(false); setInfoModal({picture, name, color, price, id})}}>
                          <img className={styles.btnDelCross} src={crossIcon} alt=""/>
                      </Button>
                      <div className={styles.image}>
                         <img className={styles.imagePicture} src={picture} alt={name}/>
                      </div>
                      <div className={styles.itemInfo}>
                         <h3 className={styles.text}>{name}</h3>
                         <p className={styles.text}>{color}</p>
                         <p className={styles.text}>{price}</p>
                         <p className={styles.text}>{id}</p>
                      </div>
                      <div className={styles.btnWrapper} >
                          <Button style={styles.btnCount} onClick={() => decrementAdd(id, countItem)}>Трошки менше</Button>
                          <span className={styles.count}>{countItem}</span>
                          <Button style={styles.btnCount} onClick={()=>{incrementAdd(info)}}>Трошки більше</Button>
                      </div>

                  </div>



    )
}

BasketItem.propTypes = {
    picture: PropTypes.string,
    name: PropTypes.string.isRequired,
    color: PropTypes.string,
    price: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    countItem: PropTypes.number.isRequired,
    decrementAdd: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired,
    incrementAdd: PropTypes.func.isRequired,
    setInfoModal: PropTypes.func.isRequired,
    setModalType: PropTypes.func.isRequired,
}
BasketItem.defaultProps = {
    picture: "",
    color: "",
}

export default BasketItem