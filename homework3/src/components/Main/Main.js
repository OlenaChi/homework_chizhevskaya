import styles from './Main.module.scss'

import Card from "../Card/Card";
import PropTypes from 'prop-types';

const Main =({data, incrementFavourite, toggleModal, setInfoModal, setModalType })=>{


        return (
            <div className={styles.main}>
                {data.map(({picture, name, color, price, id, isFavourite}) => <Card setModalType={setModalType} setInfoModal = {setInfoModal} toggleModal={toggleModal} incrementFavourite = {incrementFavourite} key = {id} picture = {picture} name = {name} color = {color} price = {price} id = {id} isFavourite = {isFavourite}/>)}
               <div  className={styles.bgi}/>
            </div>

        )

}

Main.propTypes = {
    data: PropTypes.array.isRequired,
    incrementFavourite: PropTypes.func,
    toggleModal: PropTypes.func.isRequired,


}
Main.defaultProps = {
    incrementFavourite: () =>{},
    setTitleModal: () =>{},

}
export default Main