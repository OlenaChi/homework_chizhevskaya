import styles from './Modal.module.scss';
import PropTypes from 'prop-types'
import Button from "../Button/Button";

const Modal =({ isOpenModal, modalType, toggleModal, incrementAdd, infoModal, decrementAdd}) => {
    if(!isOpenModal){
    return null
}
        return (
            <>
                <div className={styles.modal}>
                    <div  className={styles.modalBackground} onClick={()=>toggleModal(false)}/>

                    <div className={styles.modalMainContainer} >
                        <button className = {styles.btnX} onClick={()=>toggleModal(false)}>X</button>
                        <div className={styles.modalContentWrapper}>
                            {modalType && <><header className={styles.modalHeader}>
                                Подобається {infoModal.name}?
                            </header>
                                <div className={styles.modalBody}>Треба брати</div></>
                            }
                            {!modalType && <><header className={styles.modalHeader}>
                             Вже не подобається {infoModal.name}?
                            </header>
                                <div className={styles.modalBody}>Видаляємо з корзини?</div></>
                            }

                        </div>
                        <div className={styles.modalButtonWrapper}>
                            <Button style={styles.btnModal} onClick={()=>toggleModal(false)} > Ще подумаю</Button>
                            {modalType && <Button style={styles.btnModal} onClick={() => {
                                console.log(`запуск`)
                                incrementAdd(infoModal);
                                toggleModal(false);
                            }}> Авжеж беру</Button>}

                            {!modalType && <Button style={styles.btnModal} onClick={() => {
                                decrementAdd(infoModal.id);
                                toggleModal(false);
                            }}> Видаляємо</Button>}

,
                        </div>
                    </div>
                </div>


            </>
        )

}

Modal.propTypes = {
    isOpenModal: PropTypes.bool.isRequired,
    toggleModal: PropTypes.func.isRequired,
    modalType: PropTypes.bool.isRequired,
    incrementAdd: PropTypes.func.isRequired,
    infoModal:PropTypes.object.isRequired,
    decrementAdd: PropTypes.func.isRequired,
}

export default Modal;