
import {decrementListItem} from "../store/slices/listBasketSlices";
import {decrementCounterAdd} from "../store/slices/counterAddSlices";
import {setInfoModal, setIsOpenModal, setTypeOfModal} from "../store/slices/modalSlices";
import {setFavourite, setNotFavourite} from "../store/slices/dataSlices";
import {decrementCounterFavourite, incrementCounterFavourite} from "../store/slices/counterFavouriteSlices";
import {addListFavouriteItem, deleteListFavouriteItem} from "../store/slices/listFavouriteSlices";

export const decrement=(payload)=> async (dispatch)=>{
    if(payload.countItem>1){
        dispatch(decrementCounterAdd( 1))
        dispatch(decrementListItem(payload))
    }
    if(payload.countItem === 1){
        dispatch(setInfoModal(payload))
        dispatch(setIsOpenModal(true));
        dispatch(setTypeOfModal(false));
    }
}

export const setFavouriteItem = (payload)=> async(dispatch)=>{
    console.log(payload)
    if(payload.isFavourite){
        dispatch(decrementCounterFavourite())
        dispatch(deleteListFavouriteItem(payload.id))
        dispatch(setNotFavourite(payload))
    }
    if(!payload.isFavourite){
       dispatch(incrementCounterFavourite())
        dispatch(addListFavouriteItem(payload))
        dispatch(setFavourite(payload))
    }
}