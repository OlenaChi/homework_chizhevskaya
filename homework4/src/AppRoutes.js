import { Routes, Route } from 'react-router-dom'
import Main from "./components/Main/Main";
import PropTypes from 'prop-types'
import ListFavourite from "./components/Pages/ListFavourite/ListFavourite";
import ListBasket from "./components/Pages/ListBasket/ListBasket";
const AppRoutes =() =>{

    return(
        <Routes>
            <Route path = "/" element = { <Main/>} />
            <Route path = "/favourite" element = { <ListFavourite /> }/>
            <Route path = "/basket" element = {<ListBasket/>}/>
        </Routes>
    )
}
// AppRoutes.propTypes = {
//     data: PropTypes.array.isRequired,
//     incrementFavourite: PropTypes.func.isRequired,
//     toggleModal: PropTypes.func.isRequired,
//     setInfoModal:PropTypes.func.isRequired,
//     clickFavourite: PropTypes.func,
// }

export default AppRoutes