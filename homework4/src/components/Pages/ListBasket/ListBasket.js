import BasketItem from "../../BasketItem/BasketItem";
import styles from "./ListBasket.module.scss";
import PropTypes from 'prop-types';
import {shallowEqual, useSelector} from "react-redux";

const ListBasket = () => {
    const listBasket = useSelector(store => store.listBasket.listBasket, shallowEqual)
    return (<>
        <div className={styles.wrapper}>

            {listBasket.map(({picture, name, color, price, id, countItem}) => <BasketItem
                picture={picture} name = {name} color = {color} price = {price} id = {id}  key = {id} countItem={countItem}/>)}
        </div>
            <div  className={styles.bgi}/>
        </>
    )
}
//
// ListBasket.propTypes = {
//     listBasket: PropTypes.array.isRequired,
//     decrementAdd: PropTypes.func.isRequired,
//     toggleModal: PropTypes.func.isRequired,
//     incrementAdd: PropTypes.func.isRequired,
//     setInfoModal: PropTypes.func.isRequired,
//     setModalType: PropTypes.func.isRequired,
// }
export default ListBasket