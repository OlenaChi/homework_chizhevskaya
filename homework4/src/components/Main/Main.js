import styles from './Main.module.scss'
// import {store} from "../../store";
import Card from "../Card/Card";
// import PropTypes from 'prop-types';
import {shallowEqual, useDispatch, useSelector} from "react-redux";

const Main =()=>{
    const data = useSelector(store => store.data.data, shallowEqual)
    console.log(data)

        return (
            <div className={styles.main}>
                {data.map(({picture, name, color, price, id, isFavourite}) => <Card
                 key = {id} picture = {picture} name = {name} color = {color} price = {price} id = {id} isFavourite={isFavourite}/>)}
               <div  className={styles.bgi}/>
            </div>

        )

}

// Main.propTypes = {
//     data: PropTypes.array.isRequired,
//     incrementFavourite: PropTypes.func,
//     toggleModal: PropTypes.func.isRequired,
//
//
// }
// Main.defaultProps = {
//     incrementFavourite: () =>{},
//     setTitleModal: () =>{},
//
// }
export default Main