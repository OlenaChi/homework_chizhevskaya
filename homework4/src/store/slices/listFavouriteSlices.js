import {createSlice} from "@reduxjs/toolkit";
import {getListBasket, getListFavourite} from "../../api/getData";
import {setListBasket} from "./listBasketSlices";

const listFavouriteSlices = createSlice({
    name:"listFavourite",
    initialState: {
        listFavourite:[]
    },
    reducers:{
        setListFavourite:(state, action)=>{
            state.listFavourite = action.payload;

        },
        deleteListFavouriteItem:(state, action)=>{
            const index = state.listFavourite.findIndex(el => el.id === action.payload)
            state.listFavourite.splice(index, 1)
            localStorage.setItem("listFavourite", JSON.stringify(state.listFavourite))

        },
        addListFavouriteItem:(state, action)=>{
            action.payload.isFavourite=true
            state.listFavourite.push(action.payload)
            localStorage.setItem("listFavourite", JSON.stringify(state.listFavourite))
        }
    }
})

export const fetchListFavourite = ()=> async(dispatch) =>{
    const data = await getListFavourite();
    data? dispatch(setListFavourite(data)) : dispatch(setListFavourite([]))
    ;
}

export const {setListFavourite, deleteListFavouriteItem, addListFavouriteItem} = listFavouriteSlices.actions
export default listFavouriteSlices.reducer