import {createSlice} from "@reduxjs/toolkit";
import {getCounterAdd} from "../../api/getData";

const counterAddSlices = createSlice({
    name:"counterAdd",
    initialState:{
        counterAdd:0
    },
    reducers:{
        setCounterAdd:(state, action)=>{
            state.counterAdd = action.payload;

        },
        incrementCounterAdd:(state)=>{
            state.counterAdd +=1;
            localStorage.setItem('counterAdd', JSON.stringify(state.counterAdd))
        },
        decrementCounterAdd:(state,action)=>{

            state.counterAdd -= action.payload
            localStorage.setItem('counterAdd', JSON.stringify(state.counterAdd))
        }
    }
});

export const fetchCounterAdd = () => async (dispatch) => {
    const counterAdd = await getCounterAdd();
    counterAdd? dispatch(setCounterAdd(counterAdd)) : dispatch(setCounterAdd(0))


}
export default counterAddSlices.reducer
export const{setCounterAdd, incrementCounterAdd, decrementCounterAdd} = counterAddSlices.actions