import {configureStore} from "@reduxjs/toolkit";
import dataReducer from "./slices/dataSlices";
import counterAddReducer from "./slices/counterAddSlices";
import counterFavouriteReducer from "./slices/counterFavouriteSlices";
import listBasketSlicesReducer from "./slices/listBasketSlices";
import modalSlicesReducer from "./slices/modalSlices"
import listFavouriteReducer from "./slices/listFavouriteSlices"

export const store = configureStore({
    reducer:{
        data: dataReducer,
        counterAdd: counterAddReducer,
        counterFavourite: counterFavouriteReducer,
        listBasket: listBasketSlicesReducer,
        modal: modalSlicesReducer,
        listFavourite: listFavouriteReducer
    }
})
