export const getData = async () => {
    const data = localStorage.getItem('data')
    if(data){
        return JSON.parse(data)
    }
    const arrayData = await fetch(`./data.json`).then(res=>res.json());

    return arrayData.map(({ name, price, picture, id, color, isFavourite }) => ({ name, price, picture, id, color, isFavourite}))
}


export const getCounterAdd = async()=> {
    const counterAdd = localStorage.getItem('counterAdd')
    if(counterAdd){
        return JSON.parse(counterAdd)
    }
}
export const getCounterFavourite = async()=> {
    const counterFavourite = localStorage.getItem('counterFavourite')
    if(counterFavourite){
        return JSON.parse(counterFavourite)
    }
}

export const getListBasket = async()=> {
    const listBasket = localStorage.getItem('listBasket')
    if(listBasket){
        return JSON.parse(listBasket)
    }
}

export const getListFavourite = async()=> {
    const listFavourite = localStorage.getItem('listFavourite')
    if(listFavourite ){
        return JSON.parse(listFavourite )
    }
}



